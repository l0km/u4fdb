#!/bin/bash
sh_folder=$(dirname $(readlink -f $0))
cd $sh_folder 
sql2java_version=$(mvn org.apache.maven.plugins:maven-help-plugin:3.2.0:evaluate -Dexpression=sql2java.version -q -DforceStdout)
mvn com.gitee.l0km:sql2java-maven-plugin:$sql2java_version:generate -Dsql2java.classpath=lib/derby-10.13.1.1.jar -Dsql2java.propfile=gen-derby.properties || exit 255
mvn install
