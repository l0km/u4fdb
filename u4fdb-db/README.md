# 说明

本项目的数据库访问代码(ORM)由[sqlj2ava](https://gitee.com/l0km/sql2java)工具生成。

# 建表语句
sql/create_table.sql Derby数据库建表脚本

gen-sql.xml Maven脚本执行 create_table.sql

使用示例如下(需要先启动msqyl数据库):

	mvn -f gen-sql.xml sql:execute

# 生成代码

> 运行要求： JDK 1.8, Maven 3.5.0 or above running

- gen-derby.sh gen-derby.bat 用于生成接口层代码的简单脚本
- gen-derby.properties sql2java生成接口层代码的配置文件,执行脚本前务必确认文件中jdbc.drive,jdbc.url,jdbc.username,jdbc.password,jdbc.schema等参数正确配置



使用示例如下:

	$ gen-derby.bat
	[INFO] Scanning for projects...
	[INFO] 
	[INFO] ------------------------------------------------------------------------
	[INFO] Building local database for facedb apps 0.0.0-SNAPSHOT
	[INFO] ------------------------------------------------------------------------
	[INFO] 
	[INFO] --- sql2java-maven-plugin:2.1.1-SNAPSHOT:generate (default-cli) @ u4fdb-db ---
	        database properties initialization
	[INFO] classpath: [file:/J:/u4fdb/u4fdb-db/lib/derby-10.13.1.1.jar]
	Connecting to sa on jdbc:derby:J:/u4fdb/u4fdb-db/db;create=true ...
	    Connected.
	    Database server :Apache Derby.
	Loading table list according to pattern % ...
	    table GF_HOST found
	    table GF_TASK found
	    table GF_URL found
	samePrefix = [GF_]
	Loading columns ...
	        GF_HOST.HOST VARCHAR  default value: null
	        GF_HOST.SUCESS INTEGER  default value: null
	        GF_HOST.FAIL INTEGER  default value: null
	    GF_HOST found 3 columns
	        GF_TASK.MD5 CHAR  default value: null
	        GF_TASK.FILE VARCHAR  default value: null
	        GF_TASK.STATUS VARCHAR  default value: null
	        GF_TASK.VALUE INTEGER  default value: null
	        GF_TASK.EXP VARCHAR  default value: null
	    GF_TASK found 5 columns
	        GF_URL.URL VARCHAR  default value: null
	        GF_URL.STATUS VARCHAR  default value: null
	        GF_URL.MD5 CHAR  default value: null
	        GF_URL.EXP VARCHAR  default value: null
	        GF_URL.VALUE INTEGER  default value: null
	    GF_URL found 5 columns
	Database::loadPrimaryKeys
	Found primary key (seq,name) (1,HOST) for table 'GF_HOST'
	Found primary key (seq,name) (1,FILE) for table 'GF_TASK'
	Found primary key (seq,name) (1,URL) for table 'GF_URL'
	Loading imported keys ...
	Loading indexes ...
	  Found interesting index SQL200608174652270 on MD5 for table GF_TASK
	Loading procedures ...
	Generating template /templates/velocity/java5g/perschema/constant.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\Constant.java
	    java\net\facelib\u4fdb\db\Constant.java done.
	Generating template /templates/velocity/java5g/perschema/database.properties.vm
	 .... writing to src/main\resources/conf\database.properties
	    resources/conf\database.properties done.
	Generating template /templates/velocity/java5g/perschema/gu.sql2java.irowmetadata.vm
	 .... writing to src/main\resources/META-INF/services\gu.sql2java.IRowMetaData
	    resources/META-INF/services\gu.sql2java.IRowMetaData done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\HostBean.java
	    java\net\facelib\u4fdb\db\HostBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\IHostManager.java
	    java\net\facelib\u4fdb\db\IHostManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\HostMetaData.java
	    java\net\facelib\u4fdb\db\HostMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\TaskBean.java
	    java\net\facelib\u4fdb\db\TaskBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\ITaskManager.java
	    java\net\facelib\u4fdb\db\ITaskManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\TaskMetaData.java
	    java\net\facelib\u4fdb\db\TaskMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\UrlBean.java
	    java\net\facelib\u4fdb\db\UrlBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\IUrlManager.java
	    java\net\facelib\u4fdb\db\IUrlManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to src/main\java\net\facelib\u4fdb\db\UrlMetaData.java
	    java\net\facelib\u4fdb\db\UrlMetaData.java done.
	[INFO] ------------------------------------------------------------------------
	[INFO] BUILD SUCCESS
	[INFO] ------------------------------------------------------------------------
	[INFO] Total time: 1.551 s
	[INFO] Finished at: 2020-06-08T17:58:06+08:00
	[INFO] Final Memory: 22M/322M
	[INFO] ------------------------------------------------------------------------
	[INFO] Scanning for projects...
	[INFO] 
	[INFO] ------------------------------------------------------------------------
	[INFO] Building local database for facedb apps 0.0.0-SNAPSHOT
	[INFO] ------------------------------------------------------------------------
	[INFO] 
	[INFO] --- maven-enforcer-plugin:1.0:enforce (enforce-maven) @ u4fdb-db ---
	[INFO] 
	[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ u4fdb-db ---
	[INFO] Using 'UTF-8' encoding to copy filtered resources.
	[INFO] Copying 2 resources
	[INFO] 
	[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ u4fdb-db ---
	[INFO] Changes detected - recompiling the module!
	[INFO] Compiling 10 source files to J:\u4fdb\u4fdb-db\target\classes
	[INFO] 
	[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ u4fdb-db ---
	[INFO] Using 'UTF-8' encoding to copy filtered resources.
	[INFO] skip non existing resourceDirectory J:\u4fdb\u4fdb-db\src\test\resources
	[INFO] 
	[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ u4fdb-db ---
	[INFO] No sources to compile
	[INFO] 
	[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ u4fdb-db ---
	[INFO] Tests are skipped.
	[INFO] 
	[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ u4fdb-db ---
	[INFO] Building jar: J:\u4fdb\u4fdb-db\target\u4fdb-db-0.0.0-SNAPSHOT.jar
	[INFO] 
	[INFO] --- maven-install-plugin:2.4:install (default-install) @ u4fdb-db ---
	[INFO] Installing J:\u4fdb\u4fdb-db\target\u4fdb-db-0.0.0-SNAPSHOT.jar to J:\maven_repository\com\gitee\l0km\u4fdb-db\0.0.0-SNAPSHOT\u4fdb-db-0.0.0-SNAPSHOT.jar
	[INFO] Installing J:\u4fdb\u4fdb-db\pom.xml to J:\maven_repository\com\gitee\l0km\u4fdb-db\0.0.0-SNAPSHOT\u4fdb-db-0.0.0-SNAPSHOT.pom
	[INFO] ------------------------------------------------------------------------
	[INFO] BUILD SUCCESS
	[INFO] ------------------------------------------------------------------------
	[INFO] Total time: 1.879 s
	[INFO] Finished at: 2020-06-08T17:58:09+08:00
	[INFO] Final Memory: 22M/315M
	[INFO] ------------------------------------------------------------------------


