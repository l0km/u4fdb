CREATE TABLE gf_task(
    md5 char(32),
    file varchar(256) PRIMARY KEY ,
    status varchar(20) NOT NULl ,
    value int,
    exp varchar(256)
);
CREATE UNIQUE INDEX un_md5 on gf_task (md5);
CREATE TABLE gf_url(
    url varchar(256) PRIMARY KEY,
    status varchar(20) NOT NULl,
    md5 char(32),
    exp varchar(256),
    value int);
CREATE TABLE gf_host(
    host varchar(256) PRIMARY KEY,
    sucess int,
    fail int);

