// ______________________________________________________
// Generated by sql2java - https://github.com/10km/sql2java 
// JDBC driver used at code generation time: org.apache.derby.jdbc.AutoloadedDriver
// template: manager.interface.java.vm
// ______________________________________________________
package net.facelib.u4fdb.localdb;
import gu.sql2java.TableManager;
import gu.sql2java.exception.ObjectRetrievalException;
import gu.sql2java.exception.RuntimeDaoException;

/**
 * Interface to handle database calls (save, load, count, etc...) for the GF_TASK table.<br>
 * @author guyadong
 */
public interface ITaskManager extends TableManager<TaskBean>
{  
    //////////////////////////////////////
    // PRIMARY KEY METHODS
    //////////////////////////////////////

    //1
    /**
     * Loads a {@link TaskBean} from the GF_TASK using primary key fields.
     *
     * @param file String - PK# 1
     * @return a unique TaskBean or {@code null} if not found
     * @throws RuntimeDaoException
     */
    public TaskBean loadByPrimaryKey(String file)throws RuntimeDaoException;

    //1.1
    /**
     * Loads a {@link TaskBean} from the GF_TASK using primary key fields.
     *
     * @param file String - PK# 1
     * @return a unique TaskBean
     * @throws ObjectRetrievalException if not found
     * @throws RuntimeDaoException
     */
    public TaskBean loadByPrimaryKeyChecked(String file) throws RuntimeDaoException,ObjectRetrievalException;
    
    //1.4
    /**
     * check if contains row with primary key fields.
     * @param file String - PK# 1
     * @return true if this GF_TASK contains row with primary key fields.
     * @throws RuntimeDaoException
     */
    public boolean existsPrimaryKey(String file)throws RuntimeDaoException;
    //1.4.1
    /**
     * Check duplicated row by primary keys,if row exists throw exception
     * @param file primary keys
     * @return file
     * @throws RuntimeDaoException
     * @throws ObjectRetrievalException
     */
    public String checkDuplicate(String file)throws RuntimeDaoException,ObjectRetrievalException;
    //1.8
    /**
     * Loads {@link TaskBean} from the GF_TASK using primary key fields.
     *
     * @param keys primary keys array
     * @return list of TaskBean
     * @throws RuntimeDaoException
     */
    public java.util.List<TaskBean> loadByPrimaryKey(String... keys)throws RuntimeDaoException;
    //1.9
    /**
     * Loads {@link TaskBean} from the GF_TASK using primary key fields.
     *
     * @param keys primary keys collection
     * @return list of TaskBean
     * @throws RuntimeDaoException
     */
    public java.util.List<TaskBean> loadByPrimaryKey(java.util.Collection<String> keys)throws RuntimeDaoException;
    //2
    /**
     * Delete row according to its primary keys.<br>
     * all keys must not be null
     *
     * @param file String - PK# 1
     * @return the number of deleted rows
     * @throws RuntimeDaoException
     */
    public int deleteByPrimaryKey(String file)throws RuntimeDaoException;
    //2.2
    /**
     * Delete rows according to primary key.<br>
     *
     * @param keys primary keys array
     * @return the number of deleted rows
     * @throws RuntimeDaoException
     * @see #delete(gu.sql2java.BaseBean)
     */
    public int deleteByPrimaryKey(String... keys)throws RuntimeDaoException;
    //2.3
    /**
     * Delete rows according to primary key.<br>
     *
     * @param keys primary keys collection
     * @return the number of deleted rows
     * @throws RuntimeDaoException
     * @see #delete(gu.sql2java.BaseBean)
     */
    public int deleteByPrimaryKey(java.util.Collection<String> keys)throws RuntimeDaoException;
 
     //_____________________________________________________________________
    //
    // USING INDICES
    //_____________________________________________________________________


    /**
     * Retrieves an unique TaskBean using the UN_MD5 index.
     * 
     * @param md5 the MD5 column's value filter
     * @return an TaskBean,otherwise null if not found or exists null in input arguments
     * @throws RuntimeDaoException
     */
    public TaskBean loadByIndexMd5(String md5)throws RuntimeDaoException;
    /**
     * Retrieves an unique TaskBean using the UN_MD5 index.
     * 
     * @param md5 the MD5 column's value filter. must not be null
     * @return an TaskBean
     * @throws NullPointerException exists null in input arguments
     * @throws ObjectRetrievalException if not found
     * @throws RuntimeDaoException
     */
    public TaskBean loadByIndexMd5Checked(String md5)throws RuntimeDaoException,ObjectRetrievalException;
    /**
     * Retrieves an unique TaskBean for each UN_MD5 index.
     *
     * @param indexs index array
     * @return an list of TaskBean
     * @throws RuntimeDaoException
     */
    public java.util.List<TaskBean> loadByIndexMd5(String... indexs)throws RuntimeDaoException;
    /**
     * Retrieves an unique TaskBean for each UN_MD5 index.
     *
     * @param indexs index collection
     * @return an list of TaskBean
     * @throws RuntimeDaoException
     */
    public java.util.List<TaskBean> loadByIndexMd5(java.util.Collection<String> indexs)throws RuntimeDaoException;
    /**
     * Deletes rows for each UN_MD5 index.
     *
     * @param indexs index array
     * @return the number of deleted rows
     * @throws RuntimeDaoException
     */
    public int deleteByIndexMd5(String... indexs)throws RuntimeDaoException;
    /**
     * Deletes rows for each UN_MD5 index.
     *
     * @param indexs index collection
     * @return the number of deleted rows
     * @throws RuntimeDaoException
     */
    public int deleteByIndexMd5(java.util.Collection<String> indexs)throws RuntimeDaoException;

    /**
     * Deletes rows using the UN_MD5 index.
     *
     * @param md5 the MD5 column's value filter.
     * @return the number of deleted objects
     * @throws RuntimeDaoException
     */
    public int deleteByIndexMd5(String md5)throws RuntimeDaoException;
    

    //45
    /**
     * return a primary key list from {@link TaskBean} array
     * @param beans
     * @return primary key list
     */
    public java.util.List<String> toPrimaryKeyList(TaskBean... beans);
    //46
    /**
     * return a primary key list from {@link TaskBean} collection
     * @param beans
     * @return primary key list
     */
    public java.util.List<String> toPrimaryKeyList(java.util.Collection<TaskBean> beans);

}
