package net.facelib.facedb.tools;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.BeforeClass;
import org.junit.Test;

import net.facelib.u4fdb.localdb.TaskBean;
import net.gdface.db.DatabaseManager;
import static net.gdface.db.DaoManagement.DAO;


public class DerbyTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DatabaseManager.createSingleton(new File("."), true, true);
	}

	@Test
	public void test() {
		TaskBean taskBean = TaskBean.builder().value(0).build();
		DAO.daoAddTaskIfAbsent(taskBean);
	}

}
