/**   
* @Title: FailOpenPage.java 
* @Package net.gdface.service.search 
* @Description: TODO 
* @author guyadong   
* @date 2015年5月4日 下午2:47:17 
* @version V1.0   
*/
package net.gdface.httpclient;

/**
 * 页面打开异常
 * @author guyadong
 *
 */
public class FailOpenPage extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8985600268564806433L;

	/**
	 * @param cause
	 */
	public FailOpenPage(Throwable cause) {
		super(cause);
		// TODO 自动生成的构造函数存根
	}

	/**
	 * @param message
	 * @param cause
	 */
	public FailOpenPage(String message, Throwable cause) {
		super(message, cause);
		// TODO 自动生成的构造函数存根
	}

}
