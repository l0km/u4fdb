package net.gdface.httpclient;

import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.protocol.HttpContext;

/**
 * keep-alive策略对象<br>
 * 实现{@link ConnectionKeepAliveStrategy}接口，返回固定的keep-alive时间(毫秒)
 * @author guyadong
 *
 */
public class FixedKeepAliveStrategy implements ConnectionKeepAliveStrategy {
	public static final long DEFAULT_KEEPALIVE_DURATION = 5;
	public static final FixedKeepAliveStrategy DEFAULT_5SECONDS = new FixedKeepAliveStrategy();
	private final long durationMills;
	public FixedKeepAliveStrategy(long durationSnds) {
		this(durationSnds, TimeUnit.SECONDS);
	}
	public FixedKeepAliveStrategy(long duration,TimeUnit unit) {
		this.durationMills = unit.convert(duration, TimeUnit.MILLISECONDS);
	}

	private FixedKeepAliveStrategy() {
		this(DEFAULT_KEEPALIVE_DURATION,TimeUnit.SECONDS);
	}

	@Override
	public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
		return durationMills;
	}

}
