package net.gdface.utils;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;


public class ImageUtlity {

	public static boolean isImageSuffix(String pathname) {
		Matcher m = Pattern.compile("\\.(\\w*)$").matcher(pathname.toLowerCase());
		if (m.find() && ImageUtlity.ALL_IMG_SUFFIXES.contains(m.group(1))) {
			return true;
		}
		return false;
	}

	public static final List<String> ALL_IMG_SUFFIXES = Arrays.asList(ImageIO.getReaderFileSuffixes());

}
