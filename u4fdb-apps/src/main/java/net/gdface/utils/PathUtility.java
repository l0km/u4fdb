package net.gdface.utils;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Objects;


public class PathUtility {
	public static final Logger logger = LoggerFactory.getLogger(PathUtility.class);
	public static final String FILE_SEPARATOR_DB = "/";
	public static final String FILE_SEPARATOR_SYSTEM = System.getProperties().getProperty("file.separator");

	public static String getAbsolutePath(File root,String f) {
		return new StringBuilder(root.getAbsolutePath()).append(
				f.replace(PathUtility.FILE_SEPARATOR_DB, FILE_SEPARATOR_SYSTEM)).toString();
	}
	public static String getRelativeDbPath(File root,File f) {
		try {
			if(Objects.equal(root.getCanonicalPath(), f.getCanonicalPath())){
				return ".";
			}
			if (f.getCanonicalPath().startsWith(root.getCanonicalPath()))
				return f.getCanonicalPath().substring(root.getCanonicalPath().length())
						.replace(FILE_SEPARATOR_SYSTEM, PathUtility.FILE_SEPARATOR_DB);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

}
