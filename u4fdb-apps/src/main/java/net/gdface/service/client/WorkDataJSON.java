/**   
 * @Title: WorkDataJSON.java 
 * @Package net.gdface.worker 
 * @Description: TODO 
 * @author guyadong   
 * @date 2015年5月14日 上午8:46:22 
 * @version V1.0   
 */
package net.gdface.service.client;

import java.net.URI;

import net.gdface.service.search.ImgInfo;
import net.gdface.service.search.ImgJsonDecoderManager;
import net.gdface.worker.WorkData;
import net.gdface.worker.WorkDataWithURI;

import org.json.JSONObject;

/**
 * 固定有{@link #json}对象的工作数据容器<br>
 * 
 * @see WorkData
 * @author guyadong
 *
 */
public class WorkDataJSON extends WorkDataWithURI {
	public static final String VAR_URLBEAN = "urlbean";
	public static final String VAR_TASKBEAN = "taskbean";
	public static final String VAR_CODES = "codes";
	public static final String VAR_IMGINFO = "imginfo";
	public static final String VAR_REMOTEIMAGE = "remoteimage";
	/**
	 * {@link Boolean}是否强制检测人脸
	 */
	public static final String VAR_CHECKFACE="checkface";
	/**
	 * JSON对象
	 */
	private final JSONObject json;

	public WorkDataJSON(JSONObject json) {
		super();
		assert null != json;
		this.json = json;
	}

	public JSONObject getJson() {
		return json;
	}

	@Override
	public URI getURI() {
		ImgInfo imginfo = getVariables(WorkDataJSON.VAR_IMGINFO);
		if (null != imginfo)
			return imginfo.getLocaction();
		else {
			try {
				imginfo=ImgJsonDecoderManager.getJsonDecoder(json).create(json);
				setVariables(WorkDataJSON.VAR_IMGINFO,imginfo);
				return imginfo.getLocaction();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

}
