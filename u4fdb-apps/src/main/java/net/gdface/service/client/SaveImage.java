package net.gdface.service.client;

import net.gdface.worker.WorkData;

public interface SaveImage {

	public static final int CHECK_NO = 0x00;
	public static final int CHECK_DUP_LOCAL = 0x01;
	public static final int CHECK_DUP_IN_FACEDB = 0x02;
	public static final int CHECK_FACE = 0x04;
	public static final int CHECK_DEFAULT = CHECK_DUP_LOCAL;

	/**
	 * 保存图像数据
	 * 
	 * @param woc
	 *            工作数据对象
	 */
	public abstract void save(WorkData woc);

	/**
	 * 记录工作对象({@link WorkData})状态
	 * 
	 * @param woc
	 *            工作数据对象
	 */
	public abstract void recordStatus(WorkData woc);
	/**
	 * 是否需要对图片进行保存(本地或数据库)
	 * 
	 * @param woc
	 *            工作数据对象
	 * @return
	 */
	public boolean needSave(WorkData woc);
	/**
	 * 处理已知可能发生的{@link RuntimeException}异常,抛出未处理的异常<br>
	 * 
	 * @param e
	 * @param woc
	 *            工作数据容器
	 * @throws RuntimeException
	 *             未处理的其他异常
	 */
	public void onRuntimeException(RuntimeException e, WorkData woc) throws RuntimeException;
}