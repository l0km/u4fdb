package net.gdface.service.client;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import net.gdface.worker.WorkData;

/**
 * 工作状态枚举类型<br>
 * 顺序从小到大有一定的意义
 * @author guyadong
 *
 */
public enum Status {
	NULL("NULL"), 
	DEADHOST("DEAD HOST"),
	ERRORURL("ILLEGAL URL FORMAT OR TO LONG"),	
	FOLDEROK("FIHISHED FOLDER"),
	FOLDERUN("UNFIHISHED FOLDER"),	
	NOTFOUND("NOT FOUND"), 
	IOERROR("IO ERROR"), 
	UNEXP("UNEXPECTED EXCEPTION OCCUR"),
	FAIL_DOWNLOAD("DOWNLOAD FAIL"),
	TIMEOUT("TIMEOUT"), 
	ZERODATA("ZERO LENGTH DATA"), 	 
	NOTIMAGE("NOT IMAGE"),
	UNSUPIMAGE("UNSUPPORTED IMAGE FORMAT"),
	TOO_SMALL("TOO SMALL IMG"),
	TOO_LARGE("TOO LARGE IMG"),
	FAIL_SAVELOCAL("FAIL SAVED_LOCAL IN LOCAL"),
	NEW("NEW IMAGE"), 
	SAVED_LOCAL("SAVED IN LOCAL"),	 
	FAIL_SAVEDB("FAIL SAVE TO FACEDB"), 
	NOT_FACE("NO FACE DETECTED"),
	FACE_IMG("IMG WITH FACE"),
	ERRIMAGE("IMAGE ERR RETURN FROM FACEDBSERVICE"),
	DUPINDB("DUPLICATIVE IMG IN FACEDB"),
	SAVED_INDB("SAVED IN FACEDB");
	
	private Status(String msg) {
		this.msg = msg;
	}

	private String msg;
	/**
	 * {@link SaveImage#save(WorkData)}正常返回的的状态集
	 */
	public static final Set<Status> SAVEDB_NORMAL_STATUS = new HashSet<Status>(Arrays.asList(SAVED_INDB,
			DUPINDB, NOT_FACE, ERRIMAGE)); 

	public String getMsg() {

		return msg;
	}
}