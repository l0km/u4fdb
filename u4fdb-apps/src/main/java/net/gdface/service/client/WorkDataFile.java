/**   
 * @Title: WorkDataJSON.java 
 * @Package net.gdface.worker 
 * @Description: TODO 
 * @author guyadong   
 * @date 2015年5月14日 上午8:46:22 
 * @version V1.0   
 */
package net.gdface.service.client;

import java.io.File;
import java.net.URI;

import net.gdface.utils.Assert;
import net.gdface.worker.WorkData;
import net.gdface.worker.WorkDataWithURI;

/**
 * 固定有{@link #file}对象的工作数据容器<br>
 * 
 * @see WorkData
 * @author guyadong
 *
 */
public class WorkDataFile extends WorkDataWithURI {
	public static final String VAR_TASKBEAN = "taskbean";
	public static final String VAR_CODES = "codes";
	/**
	 * File对象
	 */
	private final File file;
	public WorkDataFile(File file) {
		super();
		Assert.notNull(file, "file");;
		this.file = file;
	}

	public File getFile() {
		return file;
	}

	@Override
	public URI getURI() {
		return file.toURI();
	}

}
