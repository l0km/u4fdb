package net.gdface.service.client;

import net.gdface.service.search.RemoteImage;
import net.gdface.service.search.RemoteImage.ActionIfNoTimeup;
import net.gdface.service.search.RemoteImageWOC;
import net.gdface.worker.NoTimeupException;
import net.gdface.worker.RetryException;
import net.gdface.worker.WorkData;
import net.gdface.worker.WorkDataWithURI;

public interface FetchImage extends SaveImage {




	/**
	 * 同步方式请求图片数据
	 * 
	 * @param woc
	 *            工作数据对象
	 * @param openReferer
	 *            是否先打开引用图片的网页
	 * @param throwRetryIfFail
	 *            出错时是否抛出{@link RetryException}
	 * @param actionIfNoTimeup TODO
	 * @return 图像数据下载成功且为有效人脸图像，则返回图像数据的MD5校验码，否则返回null
	 * @throws InterruptedException
	 * @throws RetryException 出错重试
	 * @throws NoTimeupException TODO
	 */
	public abstract String fetchSync(WorkDataWithURI woc, boolean openReferer, boolean throwRetryIfFail, ActionIfNoTimeup actionIfNoTimeup) throws InterruptedException,
			RetryException, NoTimeupException;

	/**
	 * 同步方式调用{@link #fetchSync(WorkDataWithURI, boolean, boolean, ActionIfNoTimeup)} <br>
	 * 如果主机访问间隔没有达到{@code intervalMills}指定的时间就休眠,<br>
	 * 直到达到时间间隔要求
	 * 
	 * @param woc
	 *            工作数据对象
	 * @param openReferer
	 *            是否先打开引用图片的网页
	 * @param throwRetryIfFail
	 *            出错时是否抛出{@link RetryException}
	 * @return
	 * @throws RetryException
	 *             TODO
	 * @see #fetchSync(WorkDataWithURI, boolean, boolean, ActionIfNoTimeup)
	 */
	public abstract String fetchSyncSleepIfNoTimeup(WorkDataWithURI woc, boolean openReferer, boolean throwRetryIfFail)
			throws InterruptedException, RetryException;

	/**
	 * 异步方式下载图像数据<br>
	 * 如果图像数据本地已经存在，则读取本地文件直接加入保存队列<br>
	 * {@link NoTimeupException}为RuntimeException,所以上层无需捕获，可以在需要的位置显示式捕获<br>
	 * @param throwIfNotOntime 参见{@link RemoteImage#downloadAsyn(ActionIfNoTimeup, boolean, org.apache.http.concurrent.FutureCallback)}
	 * @param woc
	 *            工作数据对象
	 * 
	 * @return true 任务开始,已经提交执行中（开始异步下载或提交到保存队列）<br>
	 *         false,任务结束,没有提交执行(判断图片无需下载，也不需要保存)
	 * @throws InterruptedException 
	 * @throws NoTimeupException 
	 * @see RemoteImage#downloadAsyn(ActionIfNoTimeup, boolean, org.apache.http.concurrent.FutureCallback)
	 */
	public abstract boolean fetchAsync(ActionIfNoTimeup throwIfNotOntime, RemoteImageWOC woc) throws NoTimeupException, InterruptedException;

	/**
	 * 异步方式下载图像数据<br>
	 * 如果主机访问间隔未到，就抛出休眠，直到时间到<br>
	 * @param woc 工作数据对象
	 * @return
	 * @throws InterruptedException 
	 * @see #fetchAsync(ActionIfNoTimeup, RemoteImageWOC)
	 */
	public abstract boolean fetchAsyncSleepIfNoTimeup(RemoteImageWOC woc) throws InterruptedException;
	/**
	 *  异步方式下载图像数据<br>
	 *  如果主机访问时间间隔未到，则抛出{@link NoTimeupException}
	 * @param woc
	 * @return
	 * @throws NoTimeupException
	 * @see #fetchAsync(ActionIfNoTimeup, RemoteImageWOC)
	 */
	public abstract boolean fetchAsyncThrowIfNoTimeup(RemoteImageWOC woc) throws NoTimeupException;
	
	/**
	 * 是否需要下载该图片<br>
	 * @param woc {@link net.gdface.worker.WorkData}
	 * 
	 * @return
	 */
	public abstract boolean needDownload(WorkData woc);


	
}