package net.gdface.service.client;

public interface U4fdbConstants {
	/** 默认的 FaceDb 服务主机名 */
	public static final String DEFAULT_FACEDB_HOST = "127.0.0.1";
	public static final String FACEDB_HOST_OPTION_LONG ="host";
	public static final String FACEDB_HOST_OPTION_DESC ="FaceDb service host,default:127.0.0.1";
	public static final String FACEDB_PORT_OPTION_LONG ="port";
	public static final String FACEDB_PORT_OPTION_DESC ="FaceDb service port,default:";
	public static final String QUEUE_SIZE_OPTION ="q";
	public static final String QUEUE_SIZE_OPTION_LONG ="queue";
	public static final String QUEUE_SIZE_OPTION_DESC ="queue size 任务队列长度,default:";
	public static final String MAX_THREAD_OPTION ="t";
	public static final String MAX_THREAD_OPTION_LONG ="threads";
	public static final String MAX_THREAD_OPTION_DESC ="maxthread num 最大线程数目, default:";
	public static final String WORK_FOLDER_OPTION ="f";
	public static final String WORK_FOLDER_OPTION_LONG ="folder";
	public static final String WORK_FOLDER_OPTION_DESC ="root of work folder 工作根路径";
	public static final String RESEARCH_OPTION ="r";
	public static final String RESEARCH_OPTION_LONG ="research";
	public static final String RESEARCH_OPTION_DESC ="remove all record and rescan all folder,删除所有记录重新扫描所有文件";
	public static final String FACE_NUM_OPTION ="n";
	public static final String FACE_NUM_OPTION_LONG ="facenum";
	public static final String FACE_NUM_OPTION_DESC ="specialize fetch facenum default all,指定捕获人脸数目,默认0(所有)";
	public static final String MUST_MATCH_OPTION ="m";
	public static final String MUST_MATCH_OPTION_LONG ="mustmatch";
	public static final String MUST_MATCH_OPTION_DESC ="mustmatch facenum,实际捕获人脸数目必须与-n 一致";
	public static final String OVERWRITE_OPTION ="o";
	public static final String OVERWRITE_OPTION_LONG ="overwrite";
	public static final String OVERWRITE_OPTION_DESC ="overwrite database record 覆盖数据库中的记录";
	public static final String DERBY_IN_MEMORY_OPTION_LONG ="memory";
	public static final String DERBY_IN_MEMORY_OPTION_DESC ="run derby in memory,derby数据库加载到内存运行";
	public static final String CLEAN_DUPLICATION_OPTION ="c1";
	public static final String CLEAN_DUPLICATION_OPTION_LONG ="cleanDuplication";
	public static final String CLEAN_DUPLICATION_OPTION_DESC ="clean duplicated image in local derby 删除本地重复文件标志";
	public static final String CLEAN_NOFACE_OPTION ="c2";
	public static final String CLEAN_NOFACE_OPTION_LONG ="cleanNoface";
	public static final String CLEAN_NOFACE_OPTION_DESC ="clean no face file ";
	public static final String CLEAN_NOIMAGE_OPTION ="c3";
	public static final String CLEAN_NOIMAGE_OPTION_LONG ="cleanNotImage";
	public static final String CLEAN_NOIMAGE_OPTION_DESC ="clean not image file";
	public static final String CLEAN_DUPINDB_OPTION ="c4";
	public static final String CLEAN_DUPINDB_OPTION_LONG ="cleanDupInDB";
	public static final String CLEAN_DUPINDB_OPTION_DESC ="clean duplicated image in facedb 删除数据库中重复文件标志";
	public static final String KEY_OPTION ="k";
	public static final String KEY_OPTION_LONG ="key";
	public static final String KEY_OPTION_DESC ="key for search image";
	public static final String SAVEDB_OPTION ="s";
	public static final String SAVEDB_OPTION_LONG ="savedb";
	public static final String SAVEDB_OPTION_DESC ="save image to facedb";
	public static final String MAXCON_OPTION_LONG ="maxcon";
	public static final String MAXCON_OPTION_DESC ="max connections for async client";

}
