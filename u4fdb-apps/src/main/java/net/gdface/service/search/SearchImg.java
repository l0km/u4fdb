package net.gdface.service.search;

import java.net.URI;
import java.util.concurrent.atomic.AtomicBoolean;

import net.gdface.httpclient.FailOpenPage;

import org.json.JSONArray;

public interface SearchImg {
	/**
	 * 从搜索引擎获取图片信息
	 * 
	 * @param srcUri
	 *            图片搜索引擎请求链接{@code URI}
	 * @param keyword
	 *            搜索关键字{@code String}
	 * @return
	 * @throws FailOpenPage
	 *             页面打开失败
	 * @throws InterruptedException
	 * @throws IllegalArgumentException  参数错误
	 */
	public abstract JSONArray getImgInfoFromSE(URI srcUri, String keyword) throws FailOpenPage, InterruptedException, IllegalArgumentException;

	/**
	 * 设置全局结束标志对象
	 */
	public abstract void setGlobalFinishedSignalObject(AtomicBoolean finished);

}