/**   
 * @Title: SaveWorkerKey.java 
 * @Package net.gdface.service.search 
 * @Description: TODO 
 * @author guyadong   
 * @date 2015年5月13日 下午9:31:41 
 * @version V1.0   
 */
package net.gdface.service.search;

import net.gdface.worker.WorkData;
import net.gdface.worker.WorkDataWithURI;

/**
 * 图像保存线程
 * 
 * @author guyadong
 *
 */
public class SaveWorkerKey extends SaveWorker {
	/**
	 * 
	 */
	public SaveWorkerKey() {
	}

	@Override
	protected void afterRecordStatus(WorkDataWithURI woc){
		WorkData.WOKING_DATA.remove(woc);		
	}
}
