/**  
 * 对360搜索中获取的json格式信息解析出需要的图像信息 
* @Title: ImgJsonDecoderImplSO360.java 
* @Package net.gdface.service.search 
* @Description: TODO 
* @author guyadong   
* @date 2015年5月14日 下午12:29:18 
* @version V1.0   
*/
package net.gdface.service.search;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author guyadong
 *
 */
public class ImgJsonDecoderImplSO360 extends ImgJsonDecoderImpl {

	/**
	 * 
	 */
	protected ImgJsonDecoderImplSO360() {
	}

	@Override
	public String getRef(JSONObject jo) throws JSONException {
		return jo.getString("link");
	}

	@Override
	public String getUrl(JSONObject jo) throws JSONException {
		return jo.getString("img");
	}

	@Override
	public int getWidth(JSONObject jo) throws JSONException {
		return jo.getInt("width");
	}

	@Override
	public int getHeight(JSONObject jo) throws JSONException {
		return jo.getInt("height");
	}

}
