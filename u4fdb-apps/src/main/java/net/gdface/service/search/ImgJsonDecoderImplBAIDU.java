/**   
* @Title: ImgJsonDecoderImplBAIDU.java 
* @Package net.gdface.service.search 
* @Description: TODO 
* @author guyadong   
* @date 2015年5月13日 下午8:34:46 
* @version V1.0   
*/
package net.gdface.service.search;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 对360搜索中获取的json格式信息解析出需要的图像信息
 * @author guyadong
 *
 */
public class ImgJsonDecoderImplBAIDU extends ImgJsonDecoderImpl {

	/**
	 * 
	 */
	protected ImgJsonDecoderImplBAIDU() {
	}

	@Override
	public String getUrl(JSONObject jo) throws JSONException {
		return  jo.getString("objURL");
	}

	@Override
	public String getRef(JSONObject jo) throws JSONException {
		return jo.getString("fromURL");
	}

	@Override
	public int getWidth(JSONObject jo) throws JSONException {
		return jo.getInt("width");
	}

	@Override
	public int getHeight(JSONObject jo) throws JSONException {
		return jo.getInt("height");
	}

}
