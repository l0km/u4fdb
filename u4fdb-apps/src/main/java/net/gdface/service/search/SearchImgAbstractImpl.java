/**   
* @Title: SearchImgAbstractImpl.java 
* @Package net.gdface.service.search 
* @Description: TODO 
* @author guyadong   
* @date 2015年4月27日 上午10:44:50 
* @version V1.0   
*/
package net.gdface.service.search;

import java.net.URI;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.json.JSONException;
import org.json.JSONObject;

import net.gdface.httpclient.FailOpenPage;
import net.gdface.httpclient.SyncClientManager;
import net.gdface.utils.Assert;

/**
 * @author guyadong
 *
 */
public abstract class SearchImgAbstractImpl implements SearchImg {
	public static final int SE_PAGE_INTERVAL_MILLS=5000;	
	/**
	 * 所有图片搜索循环结束标记
	 */
	protected static AtomicBoolean isFinished=null;
	@Override
	public void setGlobalFinishedSignalObject(AtomicBoolean finished) {
		Assert.notNull(finished, "finished");
		if(null==isFinished)
			isFinished=finished;
	}
	
	public static JSONObject getJSON(URI uri, RequestConfig config, long intervalMills, NameValuePair... headers) throws FailOpenPage, InterruptedException{
		try {
			return new JSONObject(SyncClientManager.getInstance().getPage(uri, config, intervalMills,headers));
		} catch (JSONException e) {
			throw new FailOpenPage(e);
		} 
		
	}
	
}
