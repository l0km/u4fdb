package net.gdface.service.search;

import java.net.URI;

/**
 * 从{@link org.json.JSONObject}从解析出来的图像下载信息对象<br>
 * 该对象只会由{@link ImgJsonDecoder}生成
 * @author guyadong
 *
 */
public class ImgInfo {
	private final int width;
	private final int height;
	private final int interval;
	private final String md5Ref;
	private final boolean retryed;
	private final URI locaction;
	private final URI referer;
	private final String keyword;
	protected ImgInfo(URI locaction, URI referer, int width, int height, int interval, String md5Ref, boolean retryed, String keyword) {
		super();
		this.width = width;
		this.height = height;
		this.interval = interval;
		this.md5Ref = md5Ref;
		this.retryed = retryed;
		this.locaction = locaction;
		this.referer = referer;
		this.keyword=keyword;
	}
	/**
	 * @return width
	 */
	public int getWidth() {
		return width;
	}
	/**
	 * @return height
	 */
	public int getHeight() {
		return height;
	}
	/**
	 * @return interval
	 */
	public int getInterval() {
		return interval;
	}
	/**
	 * @return md5Ref
	 */
	public String getMd5Ref() {
		return md5Ref;
	}
	/**
	 * @return retryed
	 */
	public boolean isRetryed() {
		return retryed;
	}
	/**
	 * @return locaction
	 */
	public URI getLocaction() {
		return locaction;
	}
	/**
	 * @return referer
	 */
	public URI getReferer() {
		return referer;
	}
	/**
	 * @return keyword
	 */
	public String getKeyword() {
		return keyword;
	}
	
}
