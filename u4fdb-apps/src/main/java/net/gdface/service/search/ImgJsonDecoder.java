/**   
* @Title: ImgJsonDecoder.java 
* @Package net.gdface.service.search 
* @Description: TODO 
* @author guyadong   
* @date 2015年5月13日 下午8:13:27 
* @version V1.0   
*/
package net.gdface.service.search;

import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 解释从搜索引擎获取的包含图像信息的{@link org.json.JSONObject}对象
 * @author guyadong
 *
 */
public interface ImgJsonDecoder {
	public static final String KEY_INTERVAL = "interval";
	public static final String KEY_RETRY = "retry";
	public static final String KEY_KEYWORD = "keyword";
	public static final String KEY_MD5REF = "md5ref";
	/**
	 * JSON解码器类名
	 */
	public static final String KEY_DECODER="jsondecoder";
	public static final ImgJsonDecoder BAIDU=new ImgJsonDecoderImplBAIDU();
	public static final ImgJsonDecoder SO360=new ImgJsonDecoderImplSO360();

	public abstract ImgInfo create(JSONObject jo) throws MalformedURLException, JSONException;


}
