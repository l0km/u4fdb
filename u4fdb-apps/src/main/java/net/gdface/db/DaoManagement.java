package net.gdface.db;

import static gu.sql2java.exception.DaoException.stripSQLException;

import java.sql.SQLIntegrityConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gu.sql2java.exception.RuntimeDaoException;
import net.facelib.u4fdb.localdb.TaskBean;
import net.gdface.u4fdb.BaseDao;
import net.gdface.utils.InterfaceContainer;

public class DaoManagement extends BaseDao {
	private static final Logger logger = LoggerFactory.getLogger(DaoManagement.class);

	public static final DaoManagement DAO = new DaoManagement();
	public DaoManagement() {
		super();
	}
	public final  InterfaceContainer<DuplicationListener<TaskBean>>  taskDuplicationListener = new InterfaceContainer<DuplicationListener<TaskBean>>(){};
	@Override
	public TaskBean daoSaveTask(TaskBean taskBean) throws RuntimeDaoException {
		for (;;) {
			try {
				return super.daoSaveTask(taskBean);
			} catch (RuntimeDaoException e) {
				//避免因为数据不同步造成的异常
				if (stripSQLException(e) instanceof SQLIntegrityConstraintViolationException) {
					if(daoExistsTask(taskBean.getFile())){
						if(taskBean.isNew()){
							logger.warn(e.toString());
							taskBean.setNew(false);
							continue;
						}else{
							// 其他字段冲突
						}
					}
					taskDuplicationListener.container.onUniqueKeyDuplicated(taskBean);
					return null;
				}
				throw e;
			}
		}
	}

}
