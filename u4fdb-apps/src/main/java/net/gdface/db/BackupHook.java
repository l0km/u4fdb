/**   
* @Title: BackupHook.java 
* @Package net.gdface.service.client 
* @Description: TODO 
* @author guyadong   
* @date 2015年5月4日 下午8:29:20 
* @version V1.0   
*/
package net.gdface.db;

/**
 * 数据库备份接口<br>
 * 当数据库开始备份时会自动调用该方法<br>
 * 
 * @author guyadong
 *
 */
public interface BackupHook{
	/**
	 * 当向数据库中回写记录时被触发
	 */
	public void onPersistDB();
}
