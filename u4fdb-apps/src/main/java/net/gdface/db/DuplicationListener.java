/**   
* @Title: TableListener.java 
* @Package net.gdface.db 
* @Description:  
* @author guyadong   
* @date 2015年5月28日 上午8:42:36 
* @version V1.0   
*/
package net.gdface.db;

import gu.sql2java.BaseRow;


/**
 * 重复记录异常侦听器
 * @author guyadong
 *
 */
public interface DuplicationListener<B extends BaseRow> {
	/**
	 * 当向数据库保存记录{@code bean}时,主键之外的唯一性约束键触发{@link java.sql.SQLIntegrityConstraintViolationException}时被触发
	 * @param bean
	 */
	public void onUniqueKeyDuplicated(B bean);
}
